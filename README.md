![UNADJUSTEDNONRAW_thumb_1.jpg](https://bitbucket.org/repo/z86yXzL/images/2576965545-UNADJUSTEDNONRAW_thumb_1.jpg)

# Vagrantfiles that provisions Decent node automatically

In this repository, Dcore node is prepared for use in Vagrant box using Vagrantfile, Ansible playbook and Docker containers. Development tools such as Docker CE will be installed automatically by Vagrant provision or Ansible playbook.

---
# Prerequisites
1. Install Vagrant
2. Install Provider

# Install Vagrant
Vagrant must first be installed on the machine you want to run it on. To make installation easier, Vagrant is distributed as a [binary package](https://www.vagrantup.com/downloads.html) for all supported platforms and architectures.

To install Vagrant, first find the appropriate package for your system and download it and then run the installer for your system. The installer will automatically add vagrant to your system path so that it will be able available in terminals.

After installing Vagrant, verify the installation worked by opening a new command prompt or console, and checking that vagrant is available:

   ``` $ vagrant -v ```
  
# Install provider #
 
Depending on Vagrant provider choose. By default, VirtualBox is the default provider for Vagrant. However, you may find after using Vagrant for some time that you prefer to use another provider as your default. In fact, this is quite common. To make this experience better, Vagrant allows specifying the default provider to use by setting the VAGRANT_DEFAULT_PROVIDER environmental variable.
Just set VAGRANT_DEFAULT_PROVIDER to the provider you wish to be the default. For example, if you use Vagrant with Parallels, you can set the environmental variable to Parallels and it will be your default.

[Vagrant plugins](https://github.com/hashicorp/vagrant/wiki/Available-Vagrant-Plugins)

# Getting started
1. Download vagrantfiles
2. Change into the desired software folder
3. Follow the README.md instructions inside the folder