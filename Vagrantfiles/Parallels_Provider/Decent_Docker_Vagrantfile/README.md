![UNADJUSTEDNONRAW_thumb_1.jpg](https://bitbucket.org/repo/z86yXzL/images/2576965545-UNADJUSTEDNONRAW_thumb_1.jpg)

# Vagrantfiles to set up Decent node for Parallels Provider

## Prerequisites
1. Install [Parallels Pro or Business](https://www.parallels.com/products/)
2. Install [Vagrant](https://www.vagrantup.com/downloads.html)
3. Install Vagrant Parallels plugin - $ vagrant plugin install vagrant-parallels
---


### Decent_Docker_Vagrantfile
Ubuntu 18.04 with Decentd Docker image

## Get started
1. download Vagrant file to project directory
2. run vagrant up - this command will download and install vagrant machine (in our case Ubuntu 18.04).	
3. type vagrant ssh — This will provide SSH into a running Vagrant machine and give you access to a shell.
4. run docker container - docker run -it --network=host decentnetwork/decentd:latest decentd