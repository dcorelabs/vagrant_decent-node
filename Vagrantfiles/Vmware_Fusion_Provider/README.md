![UNADJUSTEDNONRAW_thumb_1.jpg](https://bitbucket.org/repo/z86yXzL/images/2576965545-UNADJUSTEDNONRAW_thumb_1.jpg)

# Vagrantfiles to set up Decent node for VMWare Fusion Provider

## Prerequisites
1. Install [VMWare Fusion](https://www.vmware.com/products/fusion.html)
2. Install [Vagrant](https://www.vagrantup.com/downloads.html)
3. Install Vagrant Vmware Fusion plugin - $ vagrant plugin install vagrant--vmware-fusion
4. After installing the Vagrant VMware Desktop plugin for your system, you will need to install the license:
	vagrant plugin license vagrant-vmware-desktop ~/license.lic

	Please be sure to replace ~/license.lic with the path where you temporarily saved the downloaded license file to disk.
	
	```
	Warning! You cannot use your VMware product license as a Vagrant VMware plugin license. They are separate commercial products, each requiring their own license.
	```
---


### Decent_Docker_Vagrantfile
Ubuntu 18.04 with Decentd Docker image

## Get started
1. download Vagrant file to project directory
2. run vagrant up - this command will download and install vagrant machine (in our case Ubuntu 18.04).	
3. type vagrant ssh — This will provide SSH into a running Vagrant machine and give you access to a shell.
4. run docker container - docker run -it --network=host decentnetwork/decentd:latest decentd